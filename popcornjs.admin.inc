<?php

/**
 * @file
 * Serves administrative pages for the Popcorn.js module
 */

/**
 * Implements hook_admin().
 *
 * Respond.js administration settings
 */
function popcornjs_admin() {
  $form = array();

  $form['popcornjs_scope'] = array(
    '#type'           => 'select',
    '#title'          => t('Where should Popcorn.js be included?'),
    '#description'    => t('A script with a significant visual impact like Popcorn.js belongs in the &lt;head&gt; of a document.<br> However, in some cases it causes problems and you can move it to the footer using this configuration option.'),
    '#options'        => drupal_map_assoc(array(t('header'), t('footer'))),
    '#default_value'  => variable_get('popcornjs_scope', POPCORNJS_SCOPE_DEFAULT),
  );

  return system_settings_form($form);
}

